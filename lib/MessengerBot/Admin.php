<?php
class MessengerBot_Admin
{
    public function __construct()
    {
        add_action('admin_menu', array($this, 'menu'));   
  
    }
    public function menu()
    {
        add_menu_page(__('Messengerbot Settings', 'trds'), __('Messengerbot Settings', 'trds'), 'manage_options', 'messengerbot', array($this, 'addsettings'));
       add_submenu_page('', __('Update Settings', 'trds'), __('Update Settings', 'trds'), 'manage_options', 'updatemessenger-settings', array($this, 'updatesettings')); 
    }

    public function addsettings()
    {
        $settings = get_option(MessengerBot::OPTION_KEY . '_settings', array());
        $data = array('settings' => $settings);
        echo MessengerBot_View::render('admin_settings', $data);
    }

    public function updatesettings()
    {
        $array = array(
            'app_id' => $_POST['app_id'],
            'access_token' => $_POST['access_token'],
            'verify_token' => $_POST['verify_token'],
            'page_id' => $_POST['page_id'],
        );
        $options  = $array;
        $settings = update_option(MessengerBot::OPTION_KEY . '_settings', $options);
        $this->addMessage('Settings updated successfully');
        $this->redirectUrl(get_bloginfo('wpurl') . '/wp-admin/admin.php?page=messengerbot');
    }
    
    private function sendemail($to, $subject, $message, $from)
    {
        $headers = "From: " . strip_tags($from) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        @mail($to, $subject, $message, $headers);
    }

    private function addMessage($msg, $type = 'success')
    {
        if ($type == 'success') {
            printf(
                "<div class='updated'><p><strong>%s</strong></p></div>",
                $msg
            );
        } else {
            printf(
                "<div class='error'><p><strong>%s</strong></p></div>",
                $msg
            );
        }
    }
    private function redirectUrl($url)
    {
        //header('Location:'.$url);
        echo '<script>';
        echo 'window.location.href="' . $url . '"';
        echo '</script>';
    }

}
