<?php
class MessengerBot_Mod
{
    public function __construct()
    {
        session_start();
        $this->getMessengerNotice();

    }

    public function getMessengerNotice()
    {
        $challenge    = $_REQUEST['hub_challenge'];
        $verify_token = $_REQUEST['hub_verify_token'];

        $settings           = get_option(MessengerBot::OPTION_KEY . '_settings', array());
        $saved_verify_token = $settings['verify_token'];
        if (!$saved_verify_token || $saved_verify_token == '') {
            $saved_verify_token = 'mbot_uWPjscwv';
        }
        if ($verify_token === $saved_verify_token) {
            echo $challenge;exit;
        }
        $count  = 1;
        $params = file_get_contents('php://input');
        $params = json_decode($params, true);
        if ($params && $params['entry']) {
            foreach ($params['entry'] as $entry) {
                if ($entry && $entry['messaging']) {
                    foreach ($entry['messaging'] as $message) {
                        if (isset($message['sender']) && isset($message['sender']['id']) && isset($message['optin'])) {
                            $fb_id =  $_SESSION['mbot_fb_id'] = (int)$message['sender']['id'];
                            if ($message['sender']['id']) {
                                $this->sendFbMessage($message['optin']['ref'], $message['sender']['id']);
                                $count++;
                            }
                        } elseif (isset($message['sender']) && isset($message['sender']['id'])) {
                            $fb_id = (int)$message['sender']['id'];
                        }
                    }
                }
            }
        }

    }

    public function sendFbMessage($optin, $fb_id)
    {
        global $wpdb;
        $settings     = get_option(MessengerBot::OPTION_KEY . '_settings', array());
        $access_token = $settings['access_token'];

        $graph_url = "https://graph.facebook.com/v2.6/me/messages?access_token=" . $access_token;

        $temp = explode(':', $optin);

        //adding fb id to db
        
        $wpdb->delete("delete from ".$wpdb->prefix."mbot_fb_session where fb_id='".$fb_id."'");

        $array = array();
        $array['fb_id']	 = $fb_id;
        $array['session_id'] = $temp[2];
        $array['created']	 = date('Y-m-d H:i:s');
        $wpdb->insert($wpdb->prefix."mbot_fb_session", $array);



        if ($optin && $fb_id) {
          
            if (preg_match('/mbot_form/i', $optin)) {
            	$postData  = array();
            	$postData['recipient'] = array('id' => $fb_id);
                $optin = $temp[2];
                $text                  = 'Thank you for contacting '.'http://'.$_SERVER['HTTP_HOST'].' website, we will update you via messenger as requested';
                
                $postData['message'] = array('text' => $text);
                $postData            = json_encode($postData);

                //deleting from gravity
                $wpdb->delete($wpdb->prefix . "mbot_gravity_form", array('session_id' => $optin));
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $graph_url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
                );
                $output = curl_exec($ch);

                curl_close($ch);

            } elseif (preg_match('/mbot_woostatus/i', $optin)) {
                
            } elseif (preg_match('/mbot_woonote/i', $optin)) {

            	$wpdb->update($wpdb->prefix."mbot_fb_session", array('order_id'=>$temp[1]), array('fb_id'=> $fb_id));


                $optin = $temp[1];
                $_SESSION['mbot_order_id'] = $optin;
                $comments = get_comments( array(
                        'post_id' => $optin,
                        'orderby' => 'comment_ID',
                        'order'   => 'DESC',
                        'approve' => 'approve',
                        'type'    => 'order_note',
                    ) );
                foreach ($comments as $comment) {
                	$postData  = array();
                	$postData['recipient'] = array('id' => $fb_id);
                	$postData['message'] = array('text' => $comment->comment_content.' @ '.$comment->comment_date);
                	$postData            = json_encode($postData);
                	$ch = curl_init();
                	curl_setopt($ch, CURLOPT_URL, $graph_url);
                	curl_setopt($ch, CURLOPT_HEADER, 0);
                	curl_setopt($ch, CURLOPT_POST, 1);
                	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                	    'Content-Type: application/json',
                	    'Content-Length: ' . strlen($postData))
                	);
                	$output = curl_exec($ch);
                	curl_close($ch);		
                }


                $postData  = array();
                $postData['recipient'] = array('id' => $fb_id);
                $optin = $temp[1];
                $_SESSION['mbot_order_id'] = $optin;
                $orderDetail               = new WC_Order($optin);
                if ($orderDetail->post->post_status == 'wc-completed') {
                	$text.=' Order Status: '.$orderDetail->post->post_status.' / order id: '.$order_id.' / title: '.$orderDetail->post->post_title;
                	$postData['message'] = array('text' => $text);
                	$postData            = json_encode($postData);
                	$ch = curl_init();
                	curl_setopt($ch, CURLOPT_URL, $graph_url);
                	curl_setopt($ch, CURLOPT_HEADER, 0);
                	curl_setopt($ch, CURLOPT_POST, 1);
                	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                	    'Content-Type: application/json',
                	    'Content-Length: ' . strlen($postData))
                	);
                	$output = curl_exec($ch);
                	curl_close($ch);
                }
                
                
            }

            
        }
    }

}
