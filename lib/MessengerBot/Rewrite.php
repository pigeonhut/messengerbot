<?php
class MessengerBot_Rewrite
{
    public function __construct()
    {
        add_action('gform_confirmation', array($this, 'set_post_content'), 10, 4);
        add_shortcode('mbot_fb_auth', array($this, 'fbAuth'));
         if (
            in_array(
                'woocommerce/woocommerce.php',
                apply_filters('active_plugins', get_option('active_plugins'))
            )
        ) {
            // Put your plugin code here
            add_filter('woocommerce_order_status_changed', array($this, 'my_woocommerce_order_status_changed'), 10, 3);

            add_filter('woocommerce_new_customer_note', array($this, 'my_woocommerce_new_customer_note'), 10, 2);
        }
    }

    public function my_woocommerce_order_status_changed($order_id, $old_status, $new_status)
    {
        global $wpdb;
        $_SESSION['fb_optin'] = 'mbot_woonote:'.$order_id.':'.session_id();
           $exist =  $wpdb->get_row("select * from ".$wpdb->prefix."mbot_fb_session where session_id='".session_id()."'", ARRAY_A);
           if($exist['fb_id']) {
            $_SESSION['mbot_fb_id']=  $exist['fb_id'];
            $_SESSION['mbot_order_id']=  $exist['order_id'];
           }
        if ($_SESSION['mbot_order_id'] && $_SESSION['mbot_fb_id'] && $_SESSION['mbot_order_id'] == $order_id) {
            $settings     = get_option(MessengerBot::OPTION_KEY . '_settings', array());
            $access_token = $settings['access_token'];

            $graph_url             = "https://graph.facebook.com/v2.6/me/messages?access_token=" . $access_token;
            $orderDetail               = new WC_Order($order_id);
            $postData  = array();
            $text = '';
            $text.=' Order Status: '.$orderDetail->post->post_status.' / order id: '.$order_id.' / title: '.$orderDetail->post->post_title;
            $postData['message'] = array('text' => $text);
            $postData['recipient'] = array('id' => $_SESSION['mbot_fb_id']);
            
            $postData            = json_encode($postData);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $graph_url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($postData))
            );
            $output = curl_exec($ch);
         
            curl_close($ch);
        }

    }

    public function my_woocommerce_new_customer_note($order_id, $customer_note)
    {
        global $wpdb;
        $_SESSION['fb_optin'] = 'mbot_woonote:'.$order_id['order_id'].':'.session_id();
           $exist =  $wpdb->get_row("select * from ".$wpdb->prefix."mbot_fb_session where session_id='".session_id()."'", ARRAY_A);
           if($exist['fb_id']) {
            $_SESSION['mbot_fb_id']=  $exist['fb_id'];
            $_SESSION['mbot_order_id']=  $exist['order_id'];
           }

      
        if ($_SESSION['mbot_order_id'] && $_SESSION['mbot_fb_id'] && $order_id['order_id'] = $_SESSION['mbot_order_id']) {
            $settings     = get_option(MessengerBot::OPTION_KEY . '_settings', array());
            $access_token = $settings['access_token'];


               $graph_url             = "https://graph.facebook.com/v2.6/me/messages?access_token=" . $access_token;
                $postData              = array();
                $postData['recipient'] = array('id' => $_SESSION['mbot_fb_id']);
                $text                  = '';
                $postData['message'] = array('text' => $order_id['customer_note']);
                $postData            = json_encode($postData);
                $ch                    = curl_init();


                curl_setopt($ch, CURLOPT_URL, $graph_url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($postData))
                );
                $output = curl_exec($ch);
                curl_close($ch);
            
        }

    }

    public function set_post_content($confirmation, $form, $entry)
    {
        session_start();
        global $wpdb;
        if (preg_match('/mbot/i', $form['title'])) {

            $array = array();
            //getting the data according to label
            if ($form['fields']) {
                foreach ($form['fields'] as $key => $fields) {
                    $data[$key]['label'] = $fields['label'];
                    $data[$key]['value'] = $entry[$fields['id']];
                    $array['form_id'] = $fields['formId'];
                    $array['field_id'] = $fields['id'];
                    $array['label'] = $fields['label'];
                    $array['session_id'] = session_id();
                    $array['value'] = $entry[$fields['id']];
                    $array['created'] = date('Y-m-d H:i:s');
                    $wpdb->insert($wpdb->prefix."mbot_gravity_form", $array);
                }
            }
            $_SESSION['fb_optin'] = 'mbot_form:form:'.session_id();

        }
        return $confirmation;

    }

    public function fbAuth($atts, $content)
    {
        if(!$_SESSION['fb_optin']){
            $_SESSION['fb_optin'] = 'mbot_form:form:'.session_id();
        }

        $settings = get_option(MessengerBot::OPTION_KEY . '_settings', array());
        $data     = array('settings' => $settings,'session_id'=>$_SESSION['fb_optin']);

        echo MessengerBot_View::render('fblogin', $data);
    }

    private function redirectUrl($url)
    {
        //header('Location:'.$url);
        echo '<script>';
        echo 'window.location.href="' . $url . '"';
        echo '</script>';
    }

}
